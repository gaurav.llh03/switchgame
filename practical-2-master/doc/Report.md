# Report
I have completed the code for the switch game and played it multiple times to confirm all it's functionalities are working according to the rules.
All the unit tests are passing successfully.
Through this document I want to walk everyone through my approach of finding syntax and logical errors and fixing them.

## Fixing Syntax Errors
I ran the code and checked the error logs to know location of the syntax error.I tried solving it with my knowledge in python if possible ,else I google searched the errors and tried using suggestions of some experts.

## Fixing logical Errors
I spent a day in understanding the entire codebase to know the logic of all functions used.This helped me immensely as I was able to notice some errors in logics while understanding the code.
I found other logical errors by playing the game multiple times to confirm each rule provided and check if the game pertains to the specific rule.If it disobeyed a rule, I edited the logic to make it work correctly.

## Unit tests
After fixing all syntax and logical errors, I ran the unit tests using pytest.2 unit tests were still failing, which further helped me to realize a couple of more logical errors ,which I fixed to make the code more optimized.


Currently the entire code and unit tests are working successfully to the best of my knowledge.
Thank you!