# CHANGELOG

* v1.1.0 [2019-11-08]: Added a SmartAI computer opponent.
  Added strategy players.SmartAI
  None of the bugs have been fixed.

* v1.1.0 [2019-10-25]: First major release.
  This version is known to contain some bugs.

* v1.1.1 [2021-01-08]: 1. Error fixed.
  Syntax Error, players.py line no. 50.
  Added parameter after random.choice

* v1.1.2 [2021-01-08]: 2. Error fixed.
  Error in creating Switch object and running the game, switch.py line no. 285
  Called using run_game() instead of run_games

* v1.1.3 [2021-01-08]: 3. Error fixed.
  TypeError in switch.py line no. 53
  Instead of calling dict player_classes, call constructor of the Player class. 

* v1.1.4 [2021-01-08]: 4. Error fixed.
  At the beginning of each round, draw4 was set to true.
  Changed it to false.  

* v1.1.5 [2021-01-08]: 5. Error fixed.
  NameError in user_interface.py line no. 113.
  Changed the loop control variable from card to i.

* V1.1.6 [2021-01-08]: 6. Error fixed.
  Set can_discard of QA to true so that Q and A can be played anytime. 

* v1.1.7 [2021-01-08]: 7. Error fixed. 
  Chance alteration. ( The next players gets the turn if current player hasn't won).

* v1.1.8 [2021-01-08]: 8. Error fixed.
  If player plays Q, the next players draws 4.(It was missing)

* v1.1.9 [2021-01-08]: 9. Error fixed.
  If a player plays J, he gets to swap hands with any other player. 
  Fixed the error by adjusting the order of loop variables.

* v1.1.10 [2021-01-08]: 10. Error fixed. 
  Declared correct winner. (Earlier it was hardcoded that only 2nd player will win)

* v1.1.11 [2021-01-08]: 11. Error fixed.
  Swapped the roles of simple and smart AI. (Earlier simpleAI was assigned to smartAI and vice verca)

* v1.1.12 [2021-01-08]: 12. Error fixed.
  Assigned the roles of AI so that they play their own chance. 
  By mapping the player type to the player_classes dict and calling their respective constructors. 

* v1.1.13 [2021-01-08]: 13. Error fixed.
  Corrected the cards available. 
  Earlier 2 was used twice in line 11 of cards.py. 

* v1.1.14 [2021-01-08]: 14. Error fixed.
  Changed the value of card in test_can_discard__allows_queen in test_switch.py from K to Q. 

* v1.1.15 [2021-01-08]: All units tests successful and errors fixed. 
  This is the final version of the game. 
  The game works with the correct logic and is error free. 